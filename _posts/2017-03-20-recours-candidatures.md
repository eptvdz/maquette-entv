---
title: "Législatives: le délai des recours relatifs aux candidatures s'achève ce lundi"
date: 2017-03-20
category: national

image_url: images/news/bc76b026a10a0f250bf5d53887f4e8d9_L.jpg
---

Le délai imparti pour que les tribunaux administratifs tranchent sur les recours introduits par les partis politiques et les indépendants, à propos des candidatures aux  élections législatives du 4 mai prochain ayant fait l'objet d'une décision de rejet, s'achève ce lundi.

Aux termes de la loi organique relative au régime électoral du 25 août 2016, le rejet d'une  candidature ou d'une liste de candidats "doit être dûment motivé, selon le cas, par décision du  wali, ou du chef de la représentation diplomatique ou consulaire".

L'article 98 de cette loi stipule à son troisième alinéa que "la décision de rejet peut faire  l'objet d'un recours auprès du tribunal administratif territorialement compétent dans un délai de  trois (3) jours francs, à partir de la date de sa notification".

Ce délai est de 5 jours pour les candidats des circonscriptions électorales à l'étranger et doit  être introduit auprès du tribunal administratif d'Alger.

Selon le même article, "le tribunal administratif statue dans un délai de cinq (5) jours francs, à  compter  de la date d'enregistrement du recours" et "le jugement rendu est notifié immédiatement, par tous  les moyens

légaux, aux parties concernées, selon le cas, au wali ou au chef de la représentation diplomatique  ou consulaire, pour exécution", sachant que "le jugement n'est susceptible d'aucune voie de recours".

L'article 99 dispose que "dans le cas de rejet de candidatures au titre d'une liste, de nouvelles  candidatures peuvent être formulées dans un délai n'excédant pas le mois précédant la date du scrutin".

Selon les chiffres fournis par le ministère de l'Intérieur et des Collectivités locales, le nombre  de listes de candidats aux élections législatives est de 1.088, réparties comme suit: 797 listes  présentées par 63 partis, 163 listes d'indépendants et 128 listes issues d'alliances.

Le nombre de candidats s'élève à 12.591, soit 8.646 hommes (68,67%) et 3.945 femmes (31,33%). L'âge de la majorité des candidats (64,21 %) se situent entre 31 et 50 ans et 55,91 % ont un niveau universitaire.

Le parti du Front de libération nationale (FLN), le Rassemblement national démocratique (RND) et  Tajamou Amal El Jazair (TAJ) sont les seules formations politiques à avoir présenté des listes dans  les 48 wilayas du pays et les 4 circonscriptions électorales à l`étranger.

Le corps électoral s'élève à 23.276.550 électeurs, dont 956.534 électrices et électeurs issus de la  communauté nationale à l'étranger. Il devait être "définitivement arrêté" après la période de  recours, qui s'est achevée le 19 mars .

La campagne électorale se déroulera du 9 au 30 avril. 4.734 espaces publics ont été réservés  à l'animation des meetings.

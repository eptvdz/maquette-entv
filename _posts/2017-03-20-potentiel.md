---
title: "L’Algérie a un immense potentiel de croissance"
subtitle: Selon un communiqué la Banque mondiale
date: 2017-03-20
category: economie

image_url: images/news/405meo4wag.jpg
---

L’Algérie présente un immense potentiel de croissance et de développement, a indiqué la Banque mondiale dans un communiqué en relevant la réussite significative en matière de réduction du taux de pauvreté durant les 15 dernières années.

L’Algérie est parvenue, au cours des quinze dernières années, à faire baisser le taux de pauvreté de 20 à 7%, ce qui constitue une réussite significative, souligne le vice-président de la BM pour la région Mena, M. Hafez Ghanem, qui est actuellement en une visite en Algérie.
"Il lui faut à présent capitaliser sur ces avancées pour instaurer une société plus productive et innovante. Alors que nous avons accompagné les programmes de réforme de pays aussi divers que la Pologne, le Kazakhstan et le Viet Nam, nous aspirons à mobiliser cette expertise mondiale pour soutenir les objectifs poursuivis par l’Algérie en vue de poser les bases d’une croissance future durable", a ajouté M. Ghanem, cité dans le communiqué du Groupe de la Banque Mondiale.

Et d’ajouter, "l'Algérie a un immense potentiel et elle s’emploie actuellement à se doter d’une vision stratégique pour l’exploiter pleinement".
Au cours de sa visite de trois jours en Algérie, M. Ghanem rencontrera plusieurs membres du gouvernement ainsi que le gouverneur de la Banque d’Algérie, a précisé le communiqué de la BM.
Il s’entretiendra des progrès accomplis jusqu’ici au regard des priorités du pays et du soutien supplémentaire que la Banque, forte de son expertise mondiale, pourrait fournir à l’Algérie pour l’aider à atteindre ses objectifs.
Les discussions porteront sur les domaines dans lesquels la Banque a apporté un appui technique à l’Algérie, à savoir notamment l’amélioration des systèmes de protection sociale et l’objectif de long terme visant à réduire la dépendance du pays aux hydrocarbures et à diversifier son économie pour stimuler la croissance et la création d’emplois, a indiqué la même source.

Au cours de sa visite, M. Ghanem rencontrera également le wali d’Alger, avec lequel il abordera les enjeux liés à l’essor rapide des villes algériennes et les mesures prises pour y faire face.
Il se rendra, à cette occasion, dans la ville nouvelle intelligente de Sidi Abdallah et dialoguera également avec de jeunes entrepreneurs afin de mieux appréhender les perspectives de développement du secteur privé.
Le portefeuille du Groupe de la Banque mondiale en Algérie comprend 10 projets d’Assistance technique remboursable (ATR) portant sur des secteurs divers : l’amélioration du climat des affaires, développement de l’agriculture, renforcement des capacités statistiques pour accroître l’efficacité des systèmes de sécurité sociale.
L’ATR est un instrument par lequel des pays non emprunteurs comme l’Algérie et les pays du Golfe engagent une coopération avec la Banque internationale pour la reconstruction (BIRD) et la Société financière internationale (SFI), deux institutions spécialisées du groupe de la Banque mondiale.
SFI a investi dans trois fonds de participation privés régionaux qui financent des PME en Algérie, selon les précisions de la BM.

---
title: "Abdelwahab Derbal: 700 mille radiations du fichier électoral national"
date: 2017-03-18
category: national

image_url: images/news/fb4d8062493b23b7e81e92025f34b0f6_L.jpg
---

Le président de la Haute instance indépendante de surveillance des  élections (HIISE), Abdelwahab Derbal, a fait état, samedi à El-Oued, de 700.000 radiations du  fichier électoral national, "une première dans l’histoire de l’Algérie indépendante".

S’exprimant lors d’une rencontre avec les membres de la permanence de la HIISE de la wilaya  d’El-Oued, M. Derbal a indiqué que l’assainissement du fichier électoral constitue une condition  essentielle pour assurer la transparence des résultats des [élections législatives du 4 mai prochain](http://www.aps.dz/algerie/52785-elections-l%C3%A9gislatives-le-pr%C3%A9sident-bouteflika-convoque-le-corps-%C3%A9lectoral-pour-le-4-mai-2017).

La HIISE se dressera contre toute personne qui chercherait à  porter atteinte à l’honnêteté  des élections, car créée constitutionnellement pour faire face à toute tentative d’altérer leur  probité, a souligné M. Derbal.

Il a, dans le même sillage, appelé les membres de la permanence de la HIISE à suivre toutes  les phases de l’opération électorale, y compris le classement des listes de candidats.

Le président du HIISE, Abdelwahab Derbal, a inspecté le siège de la permanence de la HIISE  de la wilaya d’El-Oued et a eu une rencontre avec ses membres.

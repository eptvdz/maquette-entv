---
title: "Une heure pour la Planète: extinction des lumières le 25 mars à travers le territoire national"
date: 2017-03-18
category: national

image_url: images/news/9d9d125b6afefb2ea42342c46ca47039_L.jpg
---

L'Algérie s'apprête à participer à la plus grande manifestation  environnementale au monde "Une heure pour la Planète" prévue le 25 mars à travers l'extinction des  lumières de nombreux bâtiments officiels et autres sites pendant une heure, a appris l'APS auprès  des organisateurs.

Le 25 mars prochain entre 20:30 et 21:30, les principaux bâtiments publics et monuments de  la capitale et des autres wilayas du pays seront plongés dans le noir pour sensibiliser les  institutions, les administrations et le large public à l'importance de l'économie d'énergie et de la  lutte contre le réchauffement climatique, a précisé à l'APS la coordinatrice de l'événement, Ladjali  Malika.

L'événement sera supervisé par le ministère des Ressources en eau et de l'Environnement avec la participation de l'Entreprise nationale de distribution de l'électricité et du gaz (Sonelgaz), la  wilaya d'Alger, les circonscriptions administratives, les APC, le Centre de développement des  énergies renouvelables (CDER), l'Agence nationale de soutien à l'emploi des jeunes (ANSEJ) et la  société civile, représentée par le club vert pour la protection de l'environnement et l'association  "Sidra", a-t-elle précisé.

S'agissant des sites concernés par l'extinction des feux, Mme Ladjali a cité la Grande poste  d'Alger, le Bastion 23, le Mémorial du martyr (Maqam Echahid), le Palais de la culture  Moufdi-Zakaria, l'hôtel El Aurassi et la façade maritime d'Alger. Les principales artères concernées  par l'opération seront connues ultérieurement, a-t-elle ajouté.

C'est la première fois que la société civile est impliquée dans cette manifestation,  notamment les enfants, a indiqué Mme Karima Bourai, coordinatrice du groupe de travail. Ainsi,  plusieurs d'entre eux prendront part volontairement à des pièces théâtrales et autres activités et  sortiront dans les rues portant des bougies en direction de la salle Ibn Khaldoun où une soirée  symbolique sera organisée par la troupe "Imzad".

Mme Bourai a estimé nécessaire de sensibiliser les entreprises et administrations à l'importance  d'économiser l'électricité et à l'impact de cet acte sur les plans économique et financier, ajoutant  que la manifestation sera marquée par la participation de la Radio nationale à travers ses chaînes  régionales, la radio Jil FM et la l'ENTV à travers ses cinq chaînes.

La manifestation "Une heure pour la planète" qui en est à son 12e anniversaire, a été lancée en  2007 à Sydney (Australie). C'est un évènement environnemental mondial auquel la population du monde

entier y adhère en éteignant les lumières pour une durée d'une heure. L'Algérie signe de ce fait sa  participation à cette manifestation en joignant les pays arabes ayant déjà pris part à l'instar de  Dubaï et l'Arabie saoudite.

---
title: "Législatives 2017: Messahel appelle à une forte participation de la communauté nationale à l'étranger"
layout: post-video
date: 2017-03-14
category: national

image_url: images/news/9a2f338f8d676cb21b423943918f7cfd_L.jpg
video_url: https://www.youtube.com/embed/EHVkSf_MnSE
video_duration: 1h52m
---

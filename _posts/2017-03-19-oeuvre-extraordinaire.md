---
title: |
  Les Algériens ont accompli une "oeuvre extraordinaire" durant la guerre de libération
date: 2017-03-19
category: monde

image_url: images/news/50808d1e732578b001561f46fe077520_L.jpg
---

Les Algériens "ont accompli une oeuvre extraordinaire et monumentale", à  travers leur lutte de la libération nationale contre la France coloniale, en menant un combat  "courageux et héroïque", a soutenu dimanche à Alger la moudjahida, Louisa Ighilahriz.

"Grâce à nos chouhada, qui ont irrigué de leur sang notre Algérie, sans aucune peur, aucun  renoncement et aucune lassitude, que nous avons arraché notre indépendance", a-t-elle martelé, lors  d'une conférence, qu'elle a animée au siège du ministère de l'Education nationale à l'occasion du  55ème anniversaire de la fête de la Victoire qui coïncide avec la signature des accords d'Evian.

La Moudjahida a rappelé à l'assistance les souffrances et les tortures, dont elle garde toujours  les stigmates, qu'elle avait subies des geôliers de la France coloniale, soulignant que les femmes  algériennes sont restées "traumatisées à cause des affres du colonialisme".

"La France a fait également sa guerre contre la femme algérienne", a-t-elle témoigné, sous l'effet  de l'émotion, soulignant que les moudjahidine algériens avaient une "volonté tenace" de se sacrifier pour que "nos enfants vivent mieux que nous".

Evoquant la signature des accords d'Evian, la moudjadida et membre du Conseil de la nation, au  titre du tiers présidentiel, a indiqué qu'elle avait appris la nouvelle à la radio, après son  évasion d'une prison française en février 1962, soutenant que "je pleurais de joie, en entendant  cette nouvelle réjouissante".

Mme Ighilahriz a ajouté néanmoins que l'Organisation de l'armée secrète (OAS), de triste mémoire, a essayé par tous les moyens diaboliques, après la signature des accords d'Evian, de saper le moral  des Algériens, en réprimant leur joie, leur bonheur et liesse, après une nuit coloniale de 132 ans,  mais ils "n'ont pas pu le faire".

A l'issue de cette conférence, la moudjahida Louisa Ighilahriz a été distinguée par la ministre de  la l'Education nationale, Nouria Benghabrit pour sa bravoure, son courage et sa lutte durant la  guerre de libération nationale.

De son côté, Mme Benghabrit a relevé que cette date historique était une occasion pour rappeler à  la jeunesse algérienne les sacrifices de la génération de la Révolution de novembre, soulignant le rôle de l'école dans le confortement de la personnalité algérienne et la fidélité à l'unité  nationale.

"Si à cette époque, le grand défi c'était la libération du pays, actuellement c'est le savoir et  l'éducation, car ce qui caractérise aujourd'hui le monde, c'est l'économie du savoir", a-t-elle  soutenu.
